<?php

namespace Drupal\webflow;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a webflow_page entity type.
 */
interface WebflowPageInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the webflow_page name.
   *
   * @return string
   *   Name of the webflow_page.
   */
  public function getName();

  /**
   * Sets the webflow_page name.
   *
   * @param string $name
   *   The webflow_page name.
   *
   * @return \Drupal\webflow\WebflowPageInterface
   *   The called webflow_page entity.
   */
  public function setName($name);

  /**
   * Gets the webflow_page creation timestamp.
   *
   * @return int
   *   Creation timestamp of the webflow_page.
   */
  public function getCreatedTime();

  /**
   * Sets the webflow_page creation timestamp.
   *
   * @param int $timestamp
   *   The webflow_page creation timestamp.
   *
   * @return \Drupal\webflow\WebflowPageInterface
   *   The called webflow_page entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the webflow_page status.
   *
   * @return bool
   *   TRUE if the webflow_page is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the webflow_page status.
   *
   * @param bool $status
   *   TRUE to enable this webflow_page, FALSE to disable.
   *
   * @return \Drupal\webflow\WebflowPageInterface
   *   The called webflow_page entity.
   */
  public function setStatus($status);

}
