<?php

namespace Drupal\webflow\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\IntegerItem;

/**
 * Defines the 'webflow_page_id' field type.
 *
 * @FieldType(
 *   id = "webflow_page_id",
 *   label = @Translation("Webflow Page ID"),
 *   category = @Translation("General"),
 *   default_widget = "webflow_page_select",
 * )
 *
 */
class WebflowPageIdItem extends IntegerItem {

}
