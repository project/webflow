<?php

namespace Drupal\webflow\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webflow\WebflowApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'webflow_page_select' field widget.
 *
 * @FieldWidget(
 *   id = "webflow_page_select",
 *   label = @Translation("Webflow ID Select"),
 *   field_types = {"webflow_page_id"},
 * )
 */
class WebflowPageSelectWidget extends WidgetBase {
  /**
   * The Webflow API service.
   *
   * @var WebflowApi
   */
  protected $webflowApi;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, WebflowApi $webflowApi) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->webflowApi = $webflowApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('webflow.webflow_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['value'] = $element + [
        '#type' => 'select',
        '#empty_option' => $this->t('- Select -'),
        '#empty_value' => '',
        '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
        '#options' => is_array($this->buildPageList()) ? $this->buildPageList() : [],
      ];

    return $element;
  }

  /**
   * Return Webflow pages.
   *
   * @return array|FALSE
   *   Array of Webflow Pages, keyed by Page ID.
   */
  protected function buildPageList() {
    if (($pages = $this->webflowApi->getStaticPagesList()) !== FALSE) {
      $list = [];
      foreach ($pages as $key => $path) {
        $list[$key] = $path;
      }
    }
    else {
      return FALSE;
    }

    return $list;
  }

}
