<?php

namespace Drupal\webflow\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\webflow\WebflowApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;

/**
 * Returns responses for Webflow routes.
 */
class EntryPoint extends ControllerBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The HTTP client service.
   *
   * @var \Symfony\Contracts\HttpClient\HttpClientInterface
   */
  protected $httpClient;

  /**
   * The Webflow API service.
   *
   * @var WebflowApi
   */
  protected $webflow;

  /**
   * The controller constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, WebflowApi $webflow) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->webflow = $webflow;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('webflow.webflow_api')
    );
  }

  /**
   * Requests a Webflow page and responds with it's contents.
   *
   * @param null $webflow_page
   *   The Webflow page path to return.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function index($webflow_page = NULL) {
    $url = $this->buildUrl($webflow_page);
    echo $this->httpClient->get($url)->getBody();
    // @TODO: Is there a better way to do this?
    // Prevent the rest of Drupal from doing anything.
    die;
  }

  /**
   * Assembles a URL to a Webflow site's page.
   *
   * @param string $webflow_page
   *   The path for the Webflow URL.
   * @return \Drupal\Core\GeneratedUrl|string
   *   The fully qualified URL string.
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function buildUrl(string $webflow_page) {
    $site = $this->webflow->getSites()[0];
    $domain = $this->webflow->getSiteDomains($site->_id);
    return Url::fromUri($domain[$site->_id] . $webflow_page)->toString();
  }

}
