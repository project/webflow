<?php

namespace Drupal\webflow;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;

/**
 * WebflowApi service.
 */
class WebflowApi implements WebflowApiInterface {

  use StringTranslationTrait;

  const WEBFLOW_API_BASE_URL = 'https://api.webflow.com';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a WebflowApi object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritDoc}
   */
  public function getApiKey() {
    return $this->configFactory->get('webflow.settings')->get('api_key');
  }

  /**
   * Build the header options for the guzzle request to Webflow's API.
   *
   * @return string[]
   *   Associative array of headers
   */
  public function buildHeaders() {
    $token = $this->getApiKey();

    return [
      'Authorization' => "Bearer $token",
      "accept-version" => "1.0.0",
      "Content-Type" => "application/json; charset=utf-8",
    ];
  }


  /**
   * {@inheritDoc}
   */
  public function getSites() {
    try {
      $response = $this->httpClient->get('/sites', [
        'base_uri' => $this->getBaseUri(),
        'headers' => $this->buildHeaders(),
      ]);

    } catch (BadResponseException $error) {
      $message = $error->getMessage();
      $this->messenger->addError($this->t('Server returned the following error: <em>@message</em>. Please check your settings or view log for more details.', ['@message' => $message]));
      $this->logger->error('<strong>' . $message . '</strong><br />' . $error->getTraceAsString());
      return FALSE;
    }

    return json_decode($response->getBody());

  }

  /**
   * {@inheritDoc}
   */
  public function getSiteDomains(string $site_id) {
    $uri = "/sites/$site_id/domains";

    try {
      $response = $this->httpClient->get($uri, [
        'base_uri' => $this->getBaseUri(),
        'headers' => $this->buildHeaders(),
      ]);
    }
    catch (BadResponseException $error) {
      $message = $error->getMessage();
      $this->messenger->addError($this->t('Server returned the following error: <em>@message</em>. Please check your settings or view log for more details.', ['@message' => $message]));
      $this->logger->error('<strong>' . $message . '</strong><br />' . $error->getTraceAsString());
      return FALSE;
    }

    $body = json_decode($response->getBody());

    if (empty($body)) {
      // No domains may be set up. Use the default webflow subdomain.
      $sites = $this->getSites();
      $site = reset($sites);
      return [$site_id => 'https://' . $site->shortName . '.webflow.io'];
    }

    return $body;
  }

  /**
   * {@inheritDoc}
   */
  public function getStaticPagesList(string $site_domain = NULL) {

    // If no site_domain was passed we'll generate the subdomain from the sites.
    if (is_null($site_domain)) {
      $sites = $this->getSites();
      if (!$sites) {
        // No sites, don't continue.
        $this->logger->error($this->t("Failed retrieving sites. Has the API key been set?"));
        $this->messenger->addError($this->t("Failed retrieving sites. Has the API key been set?"));
        return FALSE;
      }
      // No domain specifically given so just choose the first one.
      $site = reset($sites);
      $domain = "https://" . $site->shortName . '.webflow.io';
    }

    try {
      $response = $this->httpClient->get('/static-manifest.json', [
        'base_uri' => $domain,
      ]);
    } catch (BadResponseException $error) {
      $message = $error->getMessage();
      $this->messenger->addError($this->t('Server returned the following error: <em>@message</em>. Please check your settings or view log for more details.', ['@message' => $message]));
      $this->logger->error('<strong>' . $message . '</strong><br />' . $error->getTraceAsString());
      return FALSE;
    }

    return json_decode($response->getBody());
  }

  /**
   * Returns the base Webflow API URL.
   *
   * @return string
   *   The base Webflow API URL.
   */
  protected function getBaseUri() {
    return static::WEBFLOW_API_BASE_URL;
  }

}
