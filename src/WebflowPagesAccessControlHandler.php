<?php

namespace Drupal\webflow;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the webflow_pages entity type.
 */
class WebflowPagesAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view webflow_pages');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit webflow_pages', 'administer webflow_pages'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete webflow_pages', 'administer webflow_pages'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create webflow_pages', 'administer webflow_pages'], 'OR');
  }

}
