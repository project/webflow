<?php

namespace Drupal\webflow;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a webflow_pages entity type.
 */
interface WebflowPagesInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the webflow_pages title.
   *
   * @return string
   *   Title of the webflow_pages.
   */
  public function getTitle();

  /**
   * Sets the webflow_pages title.
   *
   * @param string $title
   *   The webflow_pages title.
   *
   * @return \Drupal\webflow_pages\WebflowPagesInterface
   *   The called webflow_pages entity.
   */
  public function setTitle($title);

  /**
   * Gets the webflow_pages creation timestamp.
   *
   * @return int
   *   Creation timestamp of the webflow_pages.
   */
  public function getCreatedTime();

  /**
   * Sets the webflow_pages creation timestamp.
   *
   * @param int $timestamp
   *   The webflow_pages creation timestamp.
   *
   * @return \Drupal\webflow_pages\WebflowPagesInterface
   *   The called webflow_pages entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the webflow_pages status.
   *
   * @return bool
   *   TRUE if the webflow_pages is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the webflow_pages status.
   *
   * @param bool $status
   *   TRUE to enable this webflow_pages, FALSE to disable.
   *
   * @return \Drupal\webflow_pages\WebflowPagesInterface
   *   The called webflow_pages entity.
   */
  public function setStatus($status);

}
