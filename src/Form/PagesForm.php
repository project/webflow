<?php

namespace Drupal\webflow\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\webflow\WebflowApi;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure webflow settings for this site.
 */
class PagesForm extends ConfigFormBase {

  /**
   * The Webflow API service.
   *
   * @var WebflowApi
   */
  protected $webflow;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The path alias repository.
   *
   * @var \Drupal\path_alias\AliasRepositoryInterface
   */
  protected $pathAliasRepository;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /** @var \Drupal\Core\Routing\RouteBuilder */
  protected $routeBuilder;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->webflow = $container->get('webflow.webflow_api');
    $instance->messenger = $container->get('messenger');
    $instance->pathAliasRepository = $container->get('path_alias.repository');
    $instance->languageManager = $container->get('language_manager');
    $instance->routeBuilder = $container->get('router.builder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webflow_pages';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webflow.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $disabled = FALSE;
    if (empty($this->config('webflow.settings')->get('api_key'))) {
      $settings_page_link = Link::createFromRoute($this->t('settings page'), 'webflow.settings')->toString();
      $message = $this->t('Please add your API key on the %settings_page first.', ['%settings_page' => $settings_page_link]);
      $this->messenger->addWarning($message);
      $disabled = TRUE;
    }

    $form['redirect_header'] = [
      '#markup' => '<div class="redirect-header"><p><strong>' . $this->t('Create a new redirect') . '</strong></p><p>' . $this->t('Create redirects to serve a made-in-Webflow page in place of a Drupal page') . '</p></div>',
    ];

    $header = [
      'drupal_path' => $this->t('Drupal Path'),
      'webflow_page' => $this->t('Webflow Page'),
      'remove' => $this->t('Remove')
    ];

    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#tree' => TRUE,
      '#prefix' => '<div id="table-wrapper">',
      '#suffix' => '</div>'
    ];


    if ($form_state->getValues()) {
      $this->processMappings($form, $form_state);
    }

    $mappings = $this->config('webflow.settings')->get('path_mappings');
    foreach ($mappings as $delta => $mapping) {
      $form['table'][$delta] = $this->buildRow($delta, $mapping);
    }

    $form['add_row'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add another'),
      '#submit' => ['::addRowSubmit'],
      '#disabled' => $disabled,
      '#ajax' => [
        'callback'=> '::ajaxCallback',
        'wrapper' => 'table-wrapper',
        'effect' => 'fade'
      ],
      '#add' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
      '#disabled' => $disabled,
    ];

    // By default, render the form using system-config-form.html.twig.
    $form['#theme'] = 'system_config_form';

    return $form;
  }

  /**
   * Process function to build config values.
   *
   * @param array $form
   *   Form array
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function processMappings(array $form, FormStateInterface $form_state) {
    $mappings = $this->config('webflow.settings')->get('path_mappings');

    // Remove mappings
    foreach (array_keys(array_filter($form_state->getValue('remove_mappings', []))) as $delta) {
      unset($mappings[$delta]);
    }

    $this->config('webflow.settings')
      ->set('path_mappings', $mappings)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    // Don't validate if we're removing or adding a row.
    if (!isset($element['#remove']) && !isset($element['#add'])) {
      $language = $this->languageManager->getCurrentLanguage()->getId();
      $rows = $form_state->getValue('table');
      foreach($rows as $id => $row) {
        // Ensure the row has a value for both drupal_path and webflow_page
        if (empty($row['drupal_path'])) {
          $form_state->setError($form['table'][$id]['drupal_path'], $this->t('Path must be set.'));
        }

        if (empty($row['webflow_page'])) {
          $form_state->setError($form['table'][$id]['webflow_page'], $this->t('You must make a selection for Webflow Page.'));
        }

        // Ensure path doesn't collide with existing alias.
        $drupal_path = $row['drupal_path'];
        $found = $this->pathAliasRepository->lookupByAlias($drupal_path, $language);
        if (!is_null($found)) {
          // Found an existing path, throw error.
          $form_state->setError($form['table'][$id]['drupal_path'], $this->t('A Drupal alias exists at this path (%path). Please choose a different path.',
            ['%path' => $found['path']])
          );
        }
        // @TODO: Check if redirect module is enabled and check if any redirects collide
        // @TODO: Check to make sure the same path isn't already set in page rules
        // @TODO: Check to make sure drupal path starts with forward slash

      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @TODO: Unset row values if either the drupal_path or webflow_page are empty prior to saving
    $rows = $form_state->getValue('table');
    // If there are no rows, prevent saving an empty string.
    if (empty($rows)) {
      $rows = [];
    }

    $this->config('webflow.settings')
      ->set('path_mappings', $rows)
      ->save();

    // Rebuild routes as we don't know if any routes were added or removed.
    $this->routeBuilder->rebuild();
    parent::submitForm($form, $form_state);
  }

  /**
   * Additional form submit handler for adding a blank new row.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function addRowSubmit(array &$form, FormStateInterface &$form_state) {
    $mappings = $this->config('webflow.settings')->get('path_mappings');
    $mappings[] = [
      'drupal_path' => '',
      'webflow_page' => ''
    ];
    $this->config('webflow.settings')->set('path_mappings', $mappings);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax callback for form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  public function ajaxCallback(array &$form, FormStateInterface &$form_state) {
    return $form['table'];
  }

  /**
   * Helper method to build options of Static Pages from Webflow.
   *
   * @return array
   *   Associative array of options
   */
  private function buildStaticPageOptions() {
    $options = [];
    if (!is_null($this->config('webflow.settings')->get('api_key'))) {
      try {
        $static_pages = $this->webflow->getStaticPagesList();
      } catch (ClientException $e) {
        $this->messenger->addError("The API key you used is invalid: failed to list sites");
      }

      foreach ($static_pages as $page) {
        $options[$page] = $page === '/index.html' ? 'Home' : $page;
      }
    }

    return $options;
  }

  /**
   * Builds a single row of mapping form fields.
   *
   * @param int $delta
   *   The index order of the mapping.
   * @param array $mapping
   *   The stored config mapping of drupal_path and webflow_page
   * @return array
   *   Form API array of fields.
   */
  private function buildRow(int $delta, array $mapping) {
    // @TODO: add pattern requirement to check for forward slash.
    $row['drupal_path'] = [
      '#title' => $this->t('Drupal Path'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $mapping['drupal_path'] ?? '',
      '#element_validate' => [[$this, 'webflowPathValidate']],
      '#required' => TRUE,
    ];

    $row['webflow_page'] = [
      '#title' => $this->t('Webflow Page'),
      '#title_display' => 'invisible',
      '#type' =>  'select',
      '#options' => $this->buildStaticPageOptions(),
      '#default_value' => $mapping['webflow_page'] ?? '',
      '#empty_option' => $this->t('- Select -'),
      '#empty_value' => '',
      '#required' => TRUE
    ];

    $row['remove'] = [
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#title' => $this->t('Remove'),
      '#title_display' => 'invisible',
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'wrapper' => 'table-wrapper',
        'effect' => 'fade',
        'progress' => 'none',
      ],
      '#delta' => $delta,
      '#parents' => ['remove_mappings', $delta],
      '#remove' => TRUE,
    ];

    return $row;
  }

  /**
   * Validator callback for druapl_path element to ensure that the path is valid.
   *
   * @param $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   */
  public function webflowPathValidate($element, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Path\PathValidator $validator */
    $validator = \Drupal::service('path.validator');
    return $validator->isValid($element['#value']);
  }


}
