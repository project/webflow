<?php

namespace Drupal\webflow\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webflow\WebflowApi;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure webflow settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The Webflow API service.
   *
   * @var WebflowApi
   */
  protected $webflow;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->webflow = $container->get('webflow.webflow_api');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webflow_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webflow.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // TODO: Provide language for how and where to go to get API key.
    $form['webflow_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add Webflow API Key'),
      '#default_value' => $this->config('webflow.settings')->get('api_key'),
      '#required' => true,
    ];
    if (!is_null($this->config('webflow.settings')->get('api_key'))) {
      try {
        $response = $this->webflow->getSites();
      } catch (ClientException $e) {
        \Drupal::messenger()->addError("The API key you used is invalid: failed to list sites");
      }

      if (is_array($response)) {
        $preview_url = $response[0]->previewUrl;
        $form['preview'] = [
          '#markup' => '<img src="' . $preview_url . '"/>'
        ];
      }
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('webflow_api') === '') {
      $form_state->setErrorByName('webflow_api', $this->t('Please supply a valid Webflow API key'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('webflow.settings')
      ->set('api_key', $form_state->getValue('webflow_api'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
