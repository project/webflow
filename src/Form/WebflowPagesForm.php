<?php

namespace Drupal\webflow\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the webflow_pages entity edit forms.
 */
class WebflowPagesForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New webflow_page %label has been created.', $message_arguments));
      $this->logger('webflow')->notice('Created new webflow_page %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The webflow_page %label has been updated.', $message_arguments));
      $this->logger('webflow')->notice('Updated new webflow_page %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.webflow_page.canonical', ['webflow_page' => $entity->id()]);
  }

}
