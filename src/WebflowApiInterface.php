<?php

namespace Drupal\webflow;

interface WebflowApiInterface {

  /**
   * Returns the Webflow API key.
   *
   * @return string|null
   */
  public function getApiKey();

  /**
   * Retrieves all sites the provided access token is able to access.
   *
   * @return array
   *
   * @link https://developers.webflow.com/#list-sites
   */
  public function getSites();

  /**
   * Retrieves the site's domains from Webflow.
   *
   * @param string $site_id
   *   The site ID to retrieve domains for.
   *
   * @return array
   *
   * @link https://developers.webflow.com/#domains
   */
  public function getSiteDomains(string $site_id);

  /**
   * Requests the domain's static-manifest.json file.
   *
   * @param string|null $site_domain
   *   Optionally provided fully qualified site domain URL.
   *
   * @return mixed
   *   Array of Webflow static page paths.
   */
  public function getStaticPagesList(string $site_domain = NULL);
}
