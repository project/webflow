<?php


namespace Drupal\webflow\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\webflow\Controller\EntryPoint;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Routes implements ContainerInjectionInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Routes constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }


  /**
   * Callback for dynamically RouteCollection of Webflow routes.
   *
   * @return \Symfony\Component\Routing\RouteCollection
   */
  public function routes() {
    $routes = new RouteCollection();
    $mappings = $this->configFactory->get('webflow.settings')->get('path_mappings');
    if (!empty($mappings)) {
      foreach ($mappings as $delta => $mapping) {
        $routes->add('webflow.'. $delta, static::buildRoute($mapping));
      }

      return $routes;
    }

    return NULL;
  }

  /**
   * Builds a Route from the Webflow config path mappings.
   *
   * @param array $mapping
   *   A individual mapping from webflow config.
   *
   * @return \Symfony\Component\Routing\Route
   *   The assembled route.
   */
  protected static function buildRoute(array $mapping) {
    $route = new Route($mapping['drupal_path']);
    $route->addDefaults([RouteObjectInterface::CONTROLLER_NAME => EntryPoint::class . '::index']);
    $route->addDefaults(['webflow_page' => $mapping['webflow_page']]);
    // @TODO: Set up permissions?
    $route->setRequirement('_access', 'TRUE');
    $route->setMethods(['GET']);
    return $route;
  }

}
