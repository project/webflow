<?php

namespace Drupal\webflow\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;
use Drupal\webflow\WebflowPageInterface;

/**
 * Defines the webflow_page entity class.
 *
 * @ContentEntityType(
 *   id = "webflow_page",
 *   label = @Translation("Webflow Page"),
 *   label_collection = @Translation("Webflow Pages"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\webflow\WebflowPagesListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\webflow\WebflowPagesAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\webflow\Form\WebflowPagesForm",
 *       "edit" = "Drupal\webflow\Form\WebflowPagesForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "webflow_page",
 *   data_table = "webflow_page_field_data",
 *   revision_table = "webflow_page_revision",
 *   revision_data_table = "webflow_page_field_revision",
 *   translatable = TRUE,
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer webflow_pages",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "status" = "status"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/webflow/add",
 *     "canonical" = "/webflow_page/{webflow_page}",
 *     "edit-form" = "/admin/content/webflow/{webflow_page}/edit",
 *     "delete-form" = "/admin/content/webflow/{webflow_page}/delete",
 *     "collection" = "/admin/content/webflow-pages"
 *   },
 *   field_ui_base_route = "webflow.settings"
 * )
 */
class WebflowPage extends RevisionableContentEntityBase implements WebflowPageInterface {

  use EntityChangedTrait;

  /**
   * Defines webflow page's container base path.
   */
  const WEBFLOW_PAGE_BASE_PATH = 'webflow/pages';

  /**
   * {@inheritdoc}
   *
   * When a new webflow_page entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getWebflowPageId() {
    return $this->get('webflow_page_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the webflow_page entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Status'))
      ->setDescription(t('A boolean indicating whether the webflow_page is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the webflow_page author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the webflow_page was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the webflow_page was last edited.'));

    $fields['webflow_page_id'] = BaseFieldDefinition::create('webflow_page_id')
      ->setLabel(t('Webflow ID'))
      ->setDescription(t('Unique ID for Webflow page'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'webflow_page_select',
        'weight' => 10,
      ])
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    // Body exists as a field, but hidden from display.
    $fields['body'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Page'))
      ->setDescription(t('The body of the Webflow Page as coming from Webflow.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    // Head exists as a field, but hidden from any display.
    $fields['head'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Head'))
      ->setDescription(t('The html head of the Webflow Page as coming from Webflow.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ]);


    return $fields;
  }

  /**
   * {@inheritDoc}
   *
   * During preSave() request the selected page's HTML to parse and save.
   */
  public function preSave(EntityStorageInterface $storage) {
    $api_service = 'webflow.webflow_api';
    // 1. Retrieve the selected page from webflow
    /** @var \Drupal\webflow\WebflowApiInterface $service */
    $service = \Drupal::service($api_service);
    $pages = $service->getStaticPagesList();
    // @todo allow for choosing which site to use.
    $site = $service->getSites()[0];
    $site_id = $site->_id;
    $domain = $service->getSiteDomains($site_id);
    $page = $pages[$this->getWebflowPageId()];

    // Request the page to get it's contents
    $uri = $domain[$site_id] . $page;
    $response = \Drupal::httpClient()->get($uri);
    $contents = $response->getBody()->getContents();

    // Throw the contents into DomDocument so we can more easily split and
    // parse the page's content.
    $contents = str_replace('<!DOCTYPE html>', '', $contents);

    // Load the document up into Dom Document for parsing.
    $dom = new \DOMDocument();
    $dom->loadHTML($contents);

    // Split out the head and body for separate parsing and saving.
    $head_node = $dom->getElementsByTagName('head');
    $body_node = $dom->getElementsByTagName('body');

    $head_children = $head_node->item(0);
    $head_string = '';
    foreach ($head_children->childNodes as $child) {
      $head_string .= $dom->saveHTML($child);
    }

    $this->head->value = $head_string;
    // @TODO: Have this get set from settings.
    $this->head->format = 'unaltered_html';

    $body_children = $body_node->item(0);
    $body_string = '';
    foreach ($body_children->childNodes as $child) {
      $body_string .= $dom->saveHTML($child);
    }

    $this->body->value = $body_string;
    // @TODO: Have this get set from settings.
    $this->body->format = 'unaltered_html';

    parent::preSave($storage);
  }


}
